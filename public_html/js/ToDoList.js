$(function () {
    var APPLICATION_ID = "6B27C91C-F067-13ED-FF35-32973E337000"
        SECRET_KEY = "F89080F8-5241-8A3D-FFF0-BA7B0B1C9600"
        VERSION = "v1";
        
 Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
 
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYY");
    });
 
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
 
});

function Posts(args) {
    args = args || {};
    this. title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

function todoList() {
    var item = document.getElementById("todoInput").value
    var text = document.createTextNode(item)
    var newItem = document.createElement("li")
    newItem.appendChild(text)
    document.getElementById("todoList").appendChild(newItem)
    }